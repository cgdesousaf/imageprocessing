#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 10:16:28 2018

@author: cristopher
"""

import cv2
import numpy as np

img = cv2.imread("images/baboon.png")

cv2.namedWindow("img", cv2.WINDOW_KEEPRATIO)

cv2.namedWindow("r", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("g", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("b", cv2.WINDOW_KEEPRATIO)

cv2.imshow("img", img)
cv2.imshow("b", img[:,:,0])
cv2.imshow("g", img[:,:,1])
cv2.imshow("r", img[:,:,2])

cv2.waitKey(0)