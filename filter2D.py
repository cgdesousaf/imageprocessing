#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  1 10:27:24 2018

@author: cristopher
"""

import cv2
import numpy as np

img = cv2.imread("lena.jpg", cv2.IMREAD_GRAYSCALE)

# convert to float64

img2 = np.zeros(img.shape, dtype = np.float64)

for x in range(img.shape[0]):
    for y in range(img.shape[1]):
        img2[x][y] = np.float64(img[x][y])
        
cv2.normalize(img2, img2, 1, 0, cv2.NORM_MINMAX)
        
kx = np.array([
        [-1, 0, 1],
        [-2, 0, 2],
        [-1, 0, 1]
        ])

ky = np.array([
        [-1, -2, -1],
        [0, 0, 0],
        [1, 2, 1]
        ])
    
gx = cv2.filter2D(img2, -1, kx, cv2.BORDER_DEFAULT)
gy = cv2.filter2D(img2, -1, ky, cv2.BORDER_DEFAULT)
g = np.abs(gx) + np.abs(gy)

cv2.normalize(gy, gy, 1, 0, cv2.NORM_MINMAX)
cv2.normalize(gx, gx, 1, 0, cv2.NORM_MINMAX)
cv2.normalize(g, g, 1, 0, cv2.NORM_MINMAX)

while(True):
    cv2.imshow("img", img)
    cv2.imshow("gx", gx)
    cv2.imshow("gy", gy)
    cv2.imshow("g", g)

    k = cv2.waitKey(1)
    if k == 27:
        break
    
cv2.destroyAllWindows()
