#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 16:56:44 2018

@author: cristopher
"""

import cv2
import numpy as np

def nothing(x):
    pass

noise = cv2.imread('noisy.png', 0)

cv2.namedWindow('img', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('spectrum', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('resulting_spectrum', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('mask', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('img2', cv2.WINDOW_KEEPRATIO)

cv2.createTrackbar('inner_radius', 'mask', 0, 600, nothing)
cv2.createTrackbar('outter_radius', 'mask', 0, 600, nothing)

def createCircleMask(rows, cols, inner_radius, outter_radius):
    mask = np.zeros((rows, cols, 2), np.uint8)
    mask.fill(255)
    cv2.circle(mask, (np.int(cols/2), np.int(rows/2)), outter_radius, (0,0), -1)
    cv2.circle(mask, (np.int(cols/2), np.int(rows/2)), inner_radius, (255,255), -1)
    return mask

def magnitude_spectrum(img):
    dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)
    magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))
    cv2.normalize(magnitude_spectrum, magnitude_spectrum, 1, 0, cv2.NORM_MINMAX)
    return magnitude_spectrum

while(True):
    
    k = cv2.waitKey(500)
    if k == 27:
        break
    
    
    inner_radius = cv2.getTrackbarPos('inner_radius', 'mask')
    outter_radius = cv2.getTrackbarPos('outter_radius', 'mask')
    
    print("inner radius = " + str(inner_radius))
    print("outter radius = " + str(outter_radius))
    
    # Find the magnitude_spectrum of the noise
    spectrum = magnitude_spectrum(noise)
    
    # Create a ring mask to overlap the cosine noise
    mask = createCircleMask(noise.shape[0], noise.shape[1], 
                            inner_radius, outter_radius)
    
    dft = cv2.dft(np.float32(noise),flags = cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)
    fshift = dft_shift*mask
    f_ishift = np.fft.ifftshift(fshift)
    img_back = cv2.idft(f_ishift)
    img_back = cv2.magnitude(img_back[:,:,0],img_back[:,:,1])
    
    cv2.normalize(img_back, img_back, 1, 0, cv2.NORM_MINMAX)
    
    resulting_spectrum = magnitude_spectrum(img_back)

    cv2.imshow('img', noise)    
    cv2.imshow('spectrum', spectrum)
    cv2.imshow('resulting_spectrum', resulting_spectrum)
    cv2.imshow('mask', mask[:,:,0])
    cv2.imshow('img2', img_back)
    
cv2.destroyAllWindows()