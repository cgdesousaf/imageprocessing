#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Questão 05: Filtre a imagem "lena_noise.png" para atenuar todos os ruídos 
periódicos presentes na imagem. Salve o resultado.

Created on Tue Mar 20 10:25:14 2018

@author: cristopher
"""

import cv2
import numpy as np

def nothing(x):
    pass

noise = cv2.imread('lena_noise.png', 0)

cv2.namedWindow('img', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('spectrum', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('mask', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('img2', cv2.WINDOW_KEEPRATIO)

cv2.createTrackbar('ir1', 'mask', 0, 200, nothing)
cv2.createTrackbar('or1', 'mask', 0, 200, nothing)
cv2.createTrackbar('ir2', 'mask', 0, 200, nothing)
cv2.createTrackbar('or2', 'mask', 0, 200, nothing)
cv2.createTrackbar('ir3', 'mask', 0, 200, nothing)
cv2.createTrackbar('or3', 'mask', 0, 200, nothing)

def createCircleMask(rows, cols, inner_radius, outter_radius):
    mask = np.zeros((rows, cols, 2), np.uint8)
    mask.fill(255)
    cv2.circle(mask, (np.int(cols/2), np.int(rows/2)), outter_radius, (0,0), -1)
    cv2.circle(mask, (np.int(cols/2), np.int(rows/2)), inner_radius, (255,255), -1)
    return mask

def magnitude_spectrum(img):
    dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)
    magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))
    cv2.normalize(magnitude_spectrum, magnitude_spectrum, 1, 0, cv2.NORM_MINMAX)
    return magnitude_spectrum

while(True):    
    
    ir1 = cv2.getTrackbarPos('ir1', 'mask')
    or1 = cv2.getTrackbarPos('or1', 'mask')
    
    ir2 = cv2.getTrackbarPos('ir2', 'mask')
    or2 = cv2.getTrackbarPos('or2', 'mask')
    
    ir3 = cv2.getTrackbarPos('ir3', 'mask')
    or3 = cv2.getTrackbarPos('or3', 'mask')
    
    # Find the magnitude_spectrum of the noise
    spectrum = magnitude_spectrum(noise)
    
    # Create a ring mask to overlap the cosine noise
    mask = createCircleMask(noise.shape[0], noise.shape[1], 
                            ir1, or1)
    
    mask2 = createCircleMask(noise.shape[0], noise.shape[1], 
                            ir2, or2)
    m2 = (mask2 == 0)

    mask3 = createCircleMask(noise.shape[0], noise.shape[1], 
                            ir3, or3)
    
    m3 = (mask3 == 0)
    
    cv2.normalize(mask3, mask3, 1, 0, cv2.NORM_MINMAX)
    
    mask[m2] = 0
    mask[m3] = 0
    
    print(mask)
    
    dft = cv2.dft(np.float32(noise),flags = cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)
    fshift = dft_shift*mask
    f_ishift = np.fft.ifftshift(fshift)
    img_back = cv2.idft(f_ishift)
    img_back = cv2.magnitude(img_back[:,:,0],img_back[:,:,1])
    
    cv2.normalize(img_back, img_back, 1, 0, cv2.NORM_MINMAX)

    cv2.imshow('img', noise)    
    cv2.imshow('spectrum', spectrum)
    cv2.imshow('mask', mask[:,:,0])
    cv2.imshow('img2', img_back)
    
    k = cv2.waitKey(500)
    if k == 27:
        break
    elif k == ord('s'):
        cv2.imwrite('lena.png',img_back)
    
cv2.destroyAllWindows()