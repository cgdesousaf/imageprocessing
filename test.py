import numpy as np
import cv2

img = cv2.imread('cat.jpg', cv2.IMREAD_GRAYSCALE)

cv2.imshow('img', img)

while(True):
    if cv2.waitKey(1) == ord('q'):
        break

cv2.destroyAllWindows()